# Presentation for Seminar StatML

A short presentation on the article [Bandits with many optimal arms](https://arxiv.org/abs/2103.12452) by de Heide et al., for the StatML Student's seminar.
