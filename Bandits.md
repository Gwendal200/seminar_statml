---

marp: true
math: mathjax

footer: Séminaire des élèves 2023 - DEBAUSSART & WALKER
paginate: true

---

# Bandits avec plusieurs bras optimaux

Gwendal DEBAUSSART, Raphaël WALKER

---

## Cadre

![bg right:50%](bandit.png)

* Problème de type _Bandits_
* Machines à sous $\rightarrow$ on veut maximiser nos gains
* On permet une infinité de bras
* Une proportion des bras sont optimaux

---

## Cadre - Notations

* $\mathcal{A}$ l'ensemble des bras
* Pour $a \in \mathcal{A}$, on note $\nu_a$ (avec supporte dans $[0, 1]$) sa loi, $\mu_a$ sa moyenne
* $\mu^* = \underset{a \in \mathcal{A}}{max} \ \mu_a$
* $\mu_{sub}= \underset{a \in \mathcal{A},\ \mu_a \neq \mu^*}{sup} \mu_a$
* $\Delta = |\mu^* - \mu_{sub}|$
* $\mathcal{A} = \mathcal{A}_{sub}\cup\mathcal{A}^*$, avec $\mathcal{A}^*$ l'ensemble des bras optimaux, et $\mathcal{A}_{sub}$ le reste des bras
* $p^*$ la proportion de bras optimaux
* $T$ le nombre de round, $a_t, Y_t$ l'action et la récompense a chaque round
* $N^t_j$ le nombre de fois qu'est joué le bras $j$ au temps $t$

---

## Cadre - Problèmes

$\mathfrak{B}_{\Delta,p^*}$ ensemble des problème bandits, avec une proportions de bras obtimaux $p^*$ et un ecart entre les valeurs optimales et sub-obtimales $\Delta$.

_Regret Cumulatif_: Minimiser $R(T) = \underset{t=1}{\overset{T}{\sum}}|\mu^*-\mu_{a_t}|$

_Identification du meilleur bras_: Minimiser $e(T) = \mathbb{P}(\hat{a}_T \notin \mathcal{A}^*)$

* On va s'interesser surtout au cadre du **Regret Cumulatif**

* Performance des algos en fonction de $\Delta > 0$ et $p^*$.

---

## Quand $p^*$ est connue

* Choisir $L=\lceil 4\log(T)/(p^*\gamma^2)\rceil$ bras depuis $\mathcal{A}$

* Algorithme 'classique' de type Upper Confidence Bound (UCB)
* $U^t_a = \hat{\mu}^t_a + \sqrt{\frac{\gamma^2(1-\gamma)^{-1}}{2N^t_a}}$

<!-- On n'utilise pas $\Delta$ dans l'algorithme que l'on utilise -->

> **Théorème:**
> $$\mathbb{E}[R(T)] \leq O\bigg(\frac{\log(T)\log(1/\Delta)}{p^* \Delta} \bigg)$$

---

## Quand $p^*$ est connue: performance optimale

Avec $p^*$ et $\Delta$ assez petits:

$$ \mathbb E [R(T)] \geq O(\min(\sqrt T, \log (\Delta^2 T) / (p^* \Delta))) $$

* $\Delta > O(\sqrt{1/T})$: $O(\log T)$
* $\Delta \approx \sqrt{1/T}$: $O(\sqrt T)$.

Pour $T$ grand, $O(\log T)$ comme l'algo de Sampling-UCB.

Mais, il reste un gap de $\log(1\/Delta)$.

---

## Quand $p^*$ est inconnue

> **Théorème:** _Impossibilité d'adaption de $p^*$_
> Soient $p^*\leq \frac{1}{4}$ et $c>0$ tel que $T\geq 4(\frac{c \log(T)}{p^*\Delta^2})^2$.
> Suppose que pour tout probleme de $p^*$, notre algo fait
> $$\mathbb{E}[R(T)]\leq \frac{c\log(T)}{p^* \Delta}.$$
> Alors pour tout $q^* \leq \frac{4p^*}{c}$, il existe un problème de $q^*$ où notre algo fait
> $$\mathbb{E}[R(T)] \geq \frac{\sqrt{T}\Delta}{4}$$

---

## Preuve du Théorème

* On prend $\Delta \in (0,1/4)$

* Deux distributions $R_0$ et $R_1$ sur $\mathcal{A}$:
  * $R_0$:  
        $p_1=p^*,\,\,$ $p_2= 1- p^*$  
        $\nu_1=\mathcal{B}(1/2),\,\,$  $\nu_2 = \mathcal{B}(1/2- \Delta)$.
  * $R_1$:  
        $p_1=q^*,\,\,$  $p_2=p^*,\,\,$ $p_3=1-q^*-p^*$  
        $\nu_1=\mathcal{B}(1/2+\Delta),\,\,$ $\nu_2=\mathcal{B}(1/2),\,\,$ $\nu_3=\mathcal{B}(1/2- \Delta)$

---

## Definitions Techniques

* On peut poser $\bar{\mu} = (\bar{\mu})_{1 \leq j \leq T}$, moyennes samplées dans $R_1$
* On pose aussi $\bar{\mu}' = \bar{\mu}\mathbb{1}_{\bar{\mu}_j \in \{1/2, 1/2 - \Delta \}}+ (1/2-\Delta) \mathbb{1}_{\bar{\mu}_j \notin \{1/2, 1/2 - \Delta \}}$  
 (On note que $\bar \mu '$ est une serie de moyennes i.i.d par rapport à $R_0$)
* $\mathbb{E}_{R_0},\mathbb{E}_{R_1}$ esperance par rapport a $\bar{\mu}'$ (resp. $\bar{\mu}$)

* $\mathfrak{A}$ algorithme
* Problème _Bandits_ avec distributions de Bernouilli caracterisé par $m=(m_j)_{1\leq j \leq T}$
* $\mathbb{P}^\mathfrak{A}_m$ la distribution des samples obtenu par l'algo $\mathfrak{A}$
* $\mathbb{E}^\mathfrak{A}_m$ l'esperance associée

---

## Borner la divergence KL

$$\begin{align}\textnormal{KL}(\mathbb{E}_{R_0}\mathbb{P}_{\bar{\mu}'}^\mathfrak{A},\mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}}^\mathfrak{A})& =
\textnormal{KL}(\mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}'}^\mathfrak{A},\mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}}^\mathfrak{A}) \\
& \leq \mathbb{E}_{R_1}\bigg[\textnormal{KL}(\mathbb{P}_{\bar{\mu}'}^\mathfrak{A},\mathbb{P}_{\bar{\mu}}^\mathfrak{A}) \bigg] \\
& \leq \mathbb{E}_{R_1} \bigg[\underset{j \leq T }{\sum} \mathbb{E}_{\bar{\mu}'}^\mathfrak{A}\big[N_j^T\big] \frac{\Delta^2}{16} \mathbb{1}\{ \bar{\mu}= 1/2 + \Delta\} \bigg]\\
& = \mathbb{E}_{R_0}\bigg[\underset{j \leq T }{\sum} \mathbb{E}_{\bar{\mu}'}^\mathfrak{A}\big[N_j^T\big] \frac{\Delta^2}{16} \mathbb{1}\{ \bar{\mu}= 1/2 - \Delta\}\frac{q^*}{1-q^*} \bigg] \\
&= \frac{q^* \Delta}{8} \mathbb{E}_{R_0}[R(T)] \leq \frac{cq^*}{8p^*} \log(T) \leq \frac{1}{2}\log(T)
\end{align}$$

---

On considère l'évenement $E:=\bigg\{ \underset{j\leq T}{\sum} N^T_j \mathbb{1} \{ \bar{\mu}_j' = 1/2 \} > T/2\bigg\}$

Par hyp. sur $\mathfrak{A}$,

$\mathbb{E}_{R_0}\mathbb{P}_{\bar{\mu}'}^\mathfrak{A}(E^C) \leq \frac{c \log(T)}{p^* \Delta} \times \frac{2}{T\Delta}$

$\mathbb{E}_{R_1}R(T) \geq \mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}}^\mathfrak{A}(E) \times \frac{T \Delta}{2}$

Avec l'inégalité de Bretagnolle-Huber:
$\mathbb{E}_{R_0}\mathbb{P}_{\bar{\mu}'}^\mathfrak{A}(E^C) + \mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}}^\mathfrak{A}(E) \geq \frac{1}{2} \exp \big(-\textnormal{KL}(\mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}'}^\mathfrak{A},\mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}}^\mathfrak{A}) \big) \geq \frac{1}{2\sqrt{T}}$
$\implies \mathbb{E}_{R_1}\mathbb{P}_{\bar{\mu}}^\mathfrak{A}(E) \geq \frac{1}{2\sqrt{T}} -\frac{2c \log(T)}{p^*T \Delta^2} \geq \frac{1}{4\sqrt{T}}$

D'où $\mathbb{E}_{R_1}R(T) \geq \frac{\Delta \sqrt T}{2}$.

---

## Conclusion & Questions ouvertes

* Avoir un cadre + general :
  * $\mathcal{A}$ partitionné en $K$ distributions possibles, associées a $p_k$
  * $k^* = \underset{[K]}{\textnormal{argmax}}\ \mu_k$
  * $\Delta_k = \big( \mu_{k^*} - \mu_k \big)_{[K]}$
  * Borner le regret, avec en dependance dans la séquence $(p_k, \Delta_k)_{[K]}$

* UB = LB pour regret cumulatif ? :
  * Resultats de l'article: UB = LB * $\log(1/\Delta)$
  * Trouver un algo qui verifie le UB, ou ameliorer le UB.
